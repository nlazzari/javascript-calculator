

var CalcModule = (function(){

	// Module constants
	var CALC_FONT_SIZE  	= "28px",
		CALC_FONT_FAMILY 	= "Source Code Pro",
		DIV_BY_ZERO			= "ERR DIV/0";	//divide by zero error message
		DISPLAY_ID			= "#display";	//div id for calculator display
		MEASURE_ID			= "#measure";	//dummy span id for measuring text width

	var _inputList = [],	// stores the list of numbers and operations entered
		_textBuffer = "";	// captures character input for numerical entry

	var calc = {
		
	};

function getWidthOfText(txt, fontname, fontsize){
   // set text in hidden dummy span
  $(MEASURE_ID).text(txt);
  // set font parameters
  $(MEASURE_ID).css({
    'font-size': fontsize,
    'font-family': fontname
  });
  
  // Return width
  return $(MEASURE_ID).width();
}

function getWidthOfDiv(id){
	return $(id).width();
}

// Update the calculator's display based on the type of input it receives.
// Fit the desired display text to the width of the calculator's display
function updateDisplay(text, mode) {
		var textWidth 	 = getWidthOfText(text, CALC_FONT_FAMILY, CALC_FONT_SIZE),
			displayWidth = getWidthOfDiv(DISPLAY_ID),
			number	 	 = parseFloat(text),		//text converted to floating point
			magnitude 	 = Math.abs(number);		// magnitude (no sign) of number

		// if we are entering digits, scroll display to the left when
		// we run out of room to show the latest digit entered
		if(mode === 'input') {
			var i = 0;

			// keep removing characters from the leftmost part of the string
			// until the number fits inside the display width
			while(textWidth > displayWidth) {
				i++;
				text = text.slice(i);

				textWidth 	 = getWidthOfText(text, CALC_FONT_FAMILY, CALC_FONT_SIZE);

			}

		}

		// if we are displaying a result, decide how to round the resulting
		// number based on its magnitude
		else if(mode === 'result') {

			if( magnitude > 9999 ) {
				var i = 15;
				//console.log(" 1 ");

				//Start with a large number of significant digits to display,
				// test that it will fit in the diplay. If not, reduce the
				// number of significant digits until it will display comfortably.
				while(textWidth > displayWidth) {
					text = number.toExponential(i);
					i = i - 1;
					textWidth 	 = getWidthOfText(text, CALC_FONT_FAMILY, CALC_FONT_SIZE);
				}
			}
			else if( magnitude >= 0.0001 && magnitude <= 9999 ) {
				var i = 15;
				//console.log(" 2 ");

				//Similar to the above while(..)
				while(textWidth > displayWidth) {
					text = number.toFixed(i);
					i = i - 1;
					textWidth 	 = getWidthOfText(text, CALC_FONT_FAMILY, CALC_FONT_SIZE);
				}
			}
			else if( magnitude < .0001 ) {
				var i = 15;
				//console.log(" 3 ");

				while(textWidth > displayWidth) {
					text = number.toExponential(i);
					i = i - 1;
					textWidth 	 = getWidthOfText(text, CALC_FONT_FAMILY, CALC_FONT_SIZE);
				}
			}

		}

		// Update display
		$(DISPLAY_ID).text(text);

	}


	calc.getButtonInput = function(text) {

		// NUMERICAL input
		if(text !== 'X' && text !== '÷' && text !== '+' && text !== '-' &&
		   text !== '=' && text !== 'C' && text !== 'AC' && text !== '+/-') {

			//If there is a trailing zero not followed by a decimal point:
			if(_textBuffer === '0' && text !== '.') {
				_textBuffer = text;				//remove trailing zero from text buffer
			}
			// If there is an attempt to add a second decimal point:
			else if(text === '.' && _textBuffer.indexOf(text) !== -1 ) {
				// do nothing
			}
			// otherwise proceed to add text as usual
			else {
				_textBuffer += text;			//add numeral to the text buffer
			}	
			
			updateDisplay( _textBuffer, 'input' );	// update the display

		}
		// OPERATION input
		else if(text !== '=' && text !== 'C' && text !== 'AC' && text !== '+/-') {
			if(_textBuffer === "") {		
				_textBuffer = "0"
			}
			
			_inputList.push(_textBuffer);	// if the next character is an operation
			_inputList.push(text);			// add the buffer and current character to the list

			console.log( _textBuffer);
			console.log( _inputList );

			updateDisplay( _textBuffer + " " + text, 'input' );	// update the display
			_textBuffer = "";	// re-init the character buffer
		}
		// CLEAR ENTRY
		else if(text === 'C') {
			_textBuffer = "";				//clear the current entry buffer
			updateDisplay( "0" , 'input' );	// reset the display
		}
		// CLEAR ALL
		else if(text === 'AC') {	// clear the entire list, and buffer
			_textBuffer = "";
			_inputList = [];
			updateDisplay( "0" , 'input' );	// reset the display
		}
		//PLUS / MINUS BUTTON
		else if(text === '+/-') {
			//Add a negative sign to the front of the string if not present
			if(_textBuffer[0] !== '-' && _textBuffer !== "") {
				_textBuffer = '-' + _textBuffer;
				updateDisplay( _textBuffer, 'result' );
			}
			// ...otherwise remove the negative sign
			else if(_textBuffer !== ""){
				_textBuffer = _textBuffer.slice(1);
				updateDisplay( _textBuffer, 'result' );
			}
		}
		// CALCULATE all input
		else {
			_inputList.push(_textBuffer);
			console.log(_inputList);
			this.calcAllInput();
			
		}


	}

	calc.calcAllInput = function() {
		var num1 = parseFloat(_inputList[0]),
			num2 = null,
			operation = "";

		for(var i=0; i < _inputList.length; i+=2) {
			operation = _inputList[i+1];
			num2 = parseFloat(_inputList[i+2]);
			
			//console.log(this.num1);
			//console.log(this.num2);

			//Addition
			if(operation === '+') {
				num1 += num2;
				//console.log("adding!")
			}
			//Subtraction
			if(operation === '-') {
				num1 = num1 - num2;
			}
			//Multiplication
			if(operation === 'X') {
				num1 *=  num2;
			}
			//Division
			if(operation === '÷') {
				if(num2 !== 0) {				// catch divide by zero error
					num1 = num1 / num2;
				}
				else {
					updateDisplay( DIV_BY_ZERO, 'result' );
					_textBuffer = "";
					_inputList = [];

					return;
				}
			}



		}

		updateDisplay( num1 , 'result');

		_textBuffer = num1.toString();
		_inputList = [];
	}

	calc.listAtIndex = function(index) {
		return _inputList[index];
	}

	calc.printList = function() {
		console.log(_inputList);
	}

	return calc;


}());


//Capture all button click input
// and pass it to the Calculator Module
$("button").click(function() {
    var buttonText = $(this).text();

    CalcModule.getButtonInput(buttonText);
  
});



